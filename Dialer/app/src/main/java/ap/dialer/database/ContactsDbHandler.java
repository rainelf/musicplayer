package ap.dialer.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

import ap.dialer.models.Contact;
import ap.dialer.models.Contacts;

public class ContactsDbHandler extends SQLiteOpenHelper {

    public static final String TAG              = Contact.class.getSimpleName();
    public static final int    DATABASE_VERSION = 1;
    public static final String DATABASE_NAME    = "contacts.db";

    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_FIRST_NAME = "first_name";
    public static final String COLUMN_LAST_NAME = "last_name";
    public static final String COLUMN_PHONE_NUMBER = "phone_number";


    public static final String TABLE_CONTACTS = "contacts";


    public ContactsDbHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE `" + TABLE_CONTACTS + "` (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_FIRST_NAME + " TEXT," +
                COLUMN_LAST_NAME + " TEXT," +
                COLUMN_PHONE_NUMBER + " TEXT" +
                ");";
        Log.i(TAG, "Create: " + query);
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS `" + TABLE_CONTACTS + "`;");
        onCreate(db);
    }

    public void addContact (Contact contact) {
        if (numberAlreadyInAddressBook(contact.getPhoneNumber())) {
            return;
        }

        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_FIRST_NAME, contact.getFirstName());
            values.put(COLUMN_LAST_NAME, contact.getLastName());
            values.put(COLUMN_PHONE_NUMBER, contact.getPhoneNumber().replaceAll("\\D+",""));
            SQLiteDatabase db = getWritableDatabase();
            long id = db.insert(TABLE_CONTACTS, null, values);
            Log.i(TAG, "addContactQ: " + id + " " + contact.getFirstName() + " "
                    + contact.getLastName() + " " + contact.getPhoneNumber());
        } catch (Exception e) {
            Log.i(TAG, "addContact: " + e.toString());
        }
    }

    public void editContact(Contact contact) {
        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_FIRST_NAME, contact.getFirstName());
            values.put(COLUMN_LAST_NAME, contact.getLastName());
            values.put(COLUMN_PHONE_NUMBER, contact.getPhoneNumber().replaceAll("\\D+",""));
            SQLiteDatabase db = getWritableDatabase();
            long id = db.update(TABLE_CONTACTS, values, "_id=" + contact.getId(), null);
            Log.i(TAG, "addContact: " + id + " " + contact.getFirstName() + " "
                    + contact.getLastName() + " " + contact.getPhoneNumber());
        } catch (Exception e) {
            Log.i(TAG, "addContact: " + e.toString());
        }
    }

    public boolean numberAlreadyInAddressBook(String number) {
        number = number.replaceAll("\\D+","");
        String query = "SELECT `_id` FROM `"+TABLE_CONTACTS+"` WHERE `"+COLUMN_PHONE_NUMBER+"` = '"
                + number +"'";
        SQLiteDatabase db = getWritableDatabase();

        Cursor cursor = db.rawQuery(query, null);
        int cnt = cursor.getCount();
        cursor.close();
        return (cnt > 0);
    }

    public void removeContact(String number) {
        number = number.replaceAll("\\D+","");
        String query = "DELETE FROM `" + TABLE_CONTACTS + "` WHERE `" + COLUMN_PHONE_NUMBER + "` = '"
                + number + "'";
        SQLiteDatabase db = getWritableDatabase();
        Log.i(TAG, "removeContact: "+ query);
        db.execSQL(query);
    }

    public Contact getContactFromPhoneNumber(String number)
    {
        number = number.replaceAll("\\D+","");
        String query = "SELECT * FROM '"+TABLE_CONTACTS+"' WHERE `"+COLUMN_PHONE_NUMBER+"` = '"
                + number +"'";
        SQLiteDatabase db = getWritableDatabase();

        Cursor c = db.rawQuery(query, null);
        c.moveToFirst();
        Contact contact = new Contact();
        contact.setFirstName(c.getString(c.getColumnIndex(COLUMN_FIRST_NAME)));
        contact.setLastName(c.getString(c.getColumnIndex(COLUMN_LAST_NAME)));
        contact.setPhoneNumber(c.getString(c.getColumnIndex(COLUMN_PHONE_NUMBER)));
        contact.setId(c.getInt(c.getColumnIndex(COLUMN_ID)));
        if (contact.getId() > 0) {
            return contact;
        }

        return null;
    }

    public ArrayList<Contact> getContactsFromDatabase()
    {
        String query = "SELECT * FROM `"+TABLE_CONTACTS+"`";
        SQLiteDatabase db = getWritableDatabase();
        ArrayList<Contact> contacts = new ArrayList<Contact>();
        Cursor c = db.rawQuery(query, null);
        int cnt = c.getCount();
        if (cnt > 0) {
            c.moveToFirst();
            while (!c.isAfterLast()) {

                Contact contact = new Contact();
                contact.setFirstName(c.getString(c.getColumnIndex(COLUMN_FIRST_NAME)));
                contact.setLastName(c.getString(c.getColumnIndex(COLUMN_LAST_NAME)));
                contact.setPhoneNumber(c.getString(c.getColumnIndex(COLUMN_PHONE_NUMBER)));
                contact.setId(c.getInt(c.getColumnIndex(COLUMN_ID)));
                contacts.add(contact);
                Log.i(TAG, "getContactsFromDatabase: " + contact.toString());
                c.moveToNext();
            }
        }
        return contacts;
    }

}
