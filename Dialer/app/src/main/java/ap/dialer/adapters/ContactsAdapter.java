package ap.dialer.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import ap.dialer.R;
import ap.dialer.models.Contact;


public class ContactsAdapter extends BaseAdapter {
    LayoutInflater inflater;
    private ArrayList<Contact> contacts = new ArrayList<Contact>();

    public ContactsAdapter (Context context, ArrayList<Contact> contacts) {
        this.contacts = contacts;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public Object getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.contacts_list_layout, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.firstLine = (TextView) convertView.findViewById(R.id.firstLine);
            viewHolder.secondLine = (TextView) convertView.findViewById(R.id.secondLine);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Contact contact = this.contacts.get(position);
        String firstLine = contact.getFirstName() + " " + contact.getLastName();
        String secondLine = contact.getPhoneNumber();


        viewHolder.firstLine.setText(firstLine);
        viewHolder.secondLine.setText(secondLine);

        return convertView;
    }

    private class ViewHolder {
        TextView firstLine;
        TextView secondLine;
    }


    public void updateData(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }
}
