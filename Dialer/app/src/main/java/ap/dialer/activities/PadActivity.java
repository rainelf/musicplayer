package ap.dialer.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import ap.dialer.R;

public class PadActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = PadActivity.class.getSimpleName();
    private static final int fieldLength = 16;
    private TextView numberToDialDisplay;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pad);

        Button button0 = (Button) findViewById(R.id.button0);
        button0.setOnClickListener(this);
        Button button1 = (Button) findViewById(R.id.button1);
        button1.setOnClickListener(this);
        Button button2 = (Button) findViewById(R.id.button2);
        button2.setOnClickListener(this);
        Button button3 = (Button) findViewById(R.id.button3);
        button3.setOnClickListener(this);
        Button button4 = (Button) findViewById(R.id.button4);
        button4.setOnClickListener(this);
        Button button5 = (Button) findViewById(R.id.button5);
        button5.setOnClickListener(this);
        Button button6 = (Button) findViewById(R.id.button6);
        button6.setOnClickListener(this);
        Button button7 = (Button) findViewById(R.id.button7);
        button7.setOnClickListener(this);
        Button button8 = (Button) findViewById(R.id.button8);
        button8.setOnClickListener(this);
        Button button9 = (Button) findViewById(R.id.button9);
        button9.setOnClickListener(this);
        Button buttonSharp = (Button) findViewById(R.id.buttonSharp);
        buttonSharp.setOnClickListener(this);
        Button buttonAsterisk = (Button) findViewById(R.id.buttonAsterisk);
        buttonAsterisk.setOnClickListener(this);

        this.numberToDialDisplay = (TextView) findViewById(R.id.numberToDial);

        ImageButton call = (ImageButton) findViewById(R.id.callFromPad);
        call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToCall();
            }
        });

        ImageButton back = (ImageButton) findViewById(R.id.backspace);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String currentNumber = numberToDialDisplay.getText().toString();
                if (currentNumber != null && currentNumber.length() > 0) {
                    currentNumber = currentNumber.substring(0, currentNumber.length()-1);
                    numberToDialDisplay.setText(currentNumber);
                }
            }
        });



    }

    public void goToCall() {
        Intent goToCall = new Intent(this, CallActivity.class);
        String numberToCall = this.numberToDialDisplay.getText().toString();
        goToCall.putExtra("numberToCall", numberToCall);
        startActivity(goToCall);
    }


    @Override
    public void onClick(View v) {
        Button b = (Button)v;
        String buttonText = b.getText().toString();

        String currentNumber = this.numberToDialDisplay.getText().toString();
        int currentNumberLength = currentNumber.length();
        if (currentNumberLength > fieldLength) {
            return;
        }
        currentNumber += buttonText;
        this.numberToDialDisplay.setText(currentNumber);

    }
}
