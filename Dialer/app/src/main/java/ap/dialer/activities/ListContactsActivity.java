package ap.dialer.activities;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import ap.dialer.R;
import ap.dialer.adapters.ContactsAdapter;
import ap.dialer.database.ContactsDbHandler;
import ap.dialer.models.Contact;


public class ListContactsActivity extends AppCompatActivity
{

    private static final String TAG = ListContactsActivity.class.getSimpleName();
    private ContactsDbHandler db;
    private ListView cv;

    private final static int PERMISSIONS_READ_SIM = 102;

    ArrayList<Contact> contacts;

    private ContactsAdapter contactAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_contacts);


        db = new ContactsDbHandler(this, null, null, 1);

        contacts = db.getContactsFromDatabase();

        Log.i(TAG, "onCreate: "+ contacts.size());
        contactAdapter = new ContactsAdapter(this, contacts);
        cv = (ListView) findViewById(R.id.contactsContainer);
        cv.setAdapter(contactAdapter);
        registerForContextMenu(cv);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present
        getMenuInflater().inflate(R.menu.list_contacts, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        //handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.addContact:
                startActivity(new Intent(this, AddContactActivity.class));
                return true;
            case R.id.callPad:
                startActivity(new Intent(this, PadActivity.class));
                return true;
            case R.id.importFromSim:
                askForPermissions();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void goToCall(String phoneNumber) {
        Intent goToCall = new Intent(this, CallActivity.class);
        goToCall.putExtra("numberToCall", phoneNumber);
        startActivity(goToCall);
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.select_contact_popup, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        int clientClickedNo = info.position;
        switch (item.getItemId()) {
            case R.id.deleteContact:
                db.removeContact(contacts.get(clientClickedNo).getPhoneNumber());
                contacts = db.getContactsFromDatabase();
                Log.i(TAG, "onContextItemSelected: "+ contacts.size());
                contactAdapter.updateData(contacts);
                contactAdapter.notifyDataSetChanged();
                break;

            case R.id.callContact:
                goToCall(contacts.get(clientClickedNo).getPhoneNumber());
                break;

            case R.id.editContact:
                goToEdit(contacts.get(clientClickedNo).getPhoneNumber());
                break;
        }

        return super.onContextItemSelected(item);
    }

    private void goToEdit(String phoneNumber) {
        Intent goToEdit = new Intent(this, AddContactActivity.class);
        goToEdit.putExtra("numberToEdit", phoneNumber);
        startActivity(goToEdit);
    }



    private void askForPermissions () {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        PERMISSIONS_READ_SIM);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case PERMISSIONS_READ_SIM: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    importContactsFromSim();

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    this.finish();
                    System.exit(0);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    private void importContactsFromSim()
    {

        ContentResolver cr = getContentResolver();
        Cursor cursorSim = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);

        while (cursorSim.moveToNext()) {

            String id = cursorSim.getString(cursorSim.getColumnIndex(ContactsContract.Contacts._ID));
            String name = cursorSim.getString(cursorSim.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            if (name == null) {
                continue;
            }
            String[] nameElements = name.split(" ");
            String firstName = nameElements[0];

            Contact contact = new Contact();
            contact.setFirstName(firstName);
            String lastName = "";
            if (nameElements.length > 1) {
                lastName = nameElements[1];
                if (nameElements.length > 2) {
                    lastName += " " + nameElements[2];
                }
            }
            contact.setLastName(lastName);
            int phoneNumNo = 0;
            if (Integer.parseInt(cursorSim.getString(cursorSim.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {

                // get the phone number
                Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?",
                        new String[]{id}, null);
                while (pCur.moveToNext()) {
                    String phone = pCur.getString(
                            pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    contact.setPhoneNumber(phone);
                    if (phoneNumNo == 0) {
                        db.addContact(contact);
                    }
                }
                pCur.close();
                phoneNumNo++;
            }
        }
        contacts = db.getContactsFromDatabase();
        contactAdapter.updateData(contacts);
        contactAdapter.notifyDataSetChanged();
    }
}


