package ap.dialer.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import ap.dialer.R;
import ap.dialer.database.ContactsDbHandler;
import ap.dialer.models.Contact;

public class AddContactActivity extends AppCompatActivity {

    public ContactsDbHandler db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contact);

        db = new ContactsDbHandler(this, null, null, 1);

        Intent intent = getIntent();
        String numberToEdit = intent.getStringExtra("numberToEdit");
        Button addContact = (Button) findViewById(R.id.addContactButton);

        // Add contact
        if (numberToEdit == null) {

            addContact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String firstName = ((EditText) findViewById(R.id.contactFirstName)).getText().toString();
                    String lastName = ((EditText) findViewById(R.id.contactLastName)).getText().toString();
                    String phoneNumber = ((EditText) findViewById(R.id.contactTelephone)).getText().toString();

                    if (firstName != "" && lastName != "" && phoneNumber != "") {
                        Contact contact = new Contact();
                        contact.setFirstName(firstName);
                        contact.setLastName(lastName);
                        contact.setPhoneNumber(phoneNumber);
                        db.addContact(contact);
                        goToListContacts();
                    }
                }
            });
        } else {
            // Edit contact
            final Contact contact = db.getContactFromPhoneNumber(numberToEdit);
            if (null != contact) {
                ((EditText) findViewById(R.id.contactFirstName)).setText(contact.getFirstName());
                ((EditText) findViewById(R.id.contactLastName)).setText(contact.getLastName());
                ((EditText) findViewById(R.id.contactTelephone)).setText(contact.getPhoneNumber());

                addContact.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String firstName = ((EditText) findViewById(R.id.contactFirstName)).getText().toString();
                        String lastName = ((EditText) findViewById(R.id.contactLastName)).getText().toString();
                        String phoneNumber = ((EditText) findViewById(R.id.contactTelephone)).getText().toString();
                        Contact contactEdited = new Contact();
                        contactEdited.setFirstName(firstName);
                        contactEdited.setLastName(lastName);
                        contactEdited.setPhoneNumber(phoneNumber);
                        contactEdited.setId(contact.getId());
                        db.editContact(contactEdited);
                        goToListContacts();

                    }
                });
            }
        }

    }

    public boolean goToListContacts()
    {
        startActivity(new Intent(this, ListContactsActivity.class));
        return true;
    }
}
