package services;

import android.app.Service;
import android.content.Intent;
import android.os.Environment;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.File;
import java.util.ArrayList;

import databases.SongsDBHandler;
import models.Songs;

public class ScanMediaService extends Service {

    public static final String TAG = ScanMediaService.class.getSimpleName();
    public static final String MP3_PATTERN = ".mp3";
    public static final String SD_CARD_DIR_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
    public SongsDBHandler dbHandler;

    public ArrayList<String> files = new ArrayList<String>();

    public ScanMediaService() {
        dbHandler = new SongsDBHandler(this, null, null, 1);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        dbHandler.emptyDatabase();
        scanDirectoryThread();
        printDatabase();
        //Log.i(TAG, "onStartCommand: files found" + this.files.toString());
        return START_STICKY | START_REDELIVER_INTENT;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }


    private void scanDirectory (String path)
    {
        File pathDir =  new File(path);
        File[] allFilesInDir = pathDir.listFiles();
        if (allFilesInDir != null && allFilesInDir.length > 0) {
            for (int i = 0; i < allFilesInDir.length; i++) {
                if (allFilesInDir[i].isDirectory()) {
                    scanDirectory(allFilesInDir[i].getAbsolutePath());
                } else if (allFilesInDir[i].getName().endsWith(MP3_PATTERN)) {
                    Songs song = new Songs();
                    song.setName(allFilesInDir[i].getAbsolutePath());
                    song.setTimesPlayed(0);
                    song.setRating(0);
                    dbHandler.addSong(song);
                }
            }
        }
    }

    private void scanDirectoryThread()
    {
        // the code will run() inside the thread
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                scanDirectory(SD_CARD_DIR_PATH);
                stopSelf();
            }
        };

        new Thread(runnable).start();
    }

    public void printDatabase()
    {
        String dbString = dbHandler.databaseToString();
        Log.i(TAG, "printDatabase: " + dbString);
    }

}
