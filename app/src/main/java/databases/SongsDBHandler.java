package databases;

import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;
import android.util.Log;

import java.util.ArrayList;

import models.Songs;

public class SongsDBHandler extends SQLiteOpenHelper {

    public static final String TAG = SongsDBHandler.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "songs.db";
    public static final String TABLE_SONGS = "songs";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_ARTIST = "artist";
    public static final String COLUMN_ALBUM = "album";
    public static final String COLUMN_PATH = "path";
    public static final String COLUMN_TIMES_PLAYED = "timesPlayed";
    public static final String COLUMN_RATING = "rating";

    public SongsDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.i(TAG, "onCreate: KKK");
        String query = "CREATE TABLE " + TABLE_SONGS + "(" +
                COLUMN_ID           + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                COLUMN_NAME         + " TEXT," +
                COLUMN_ARTIST       + " TEXT," +
                COLUMN_ALBUM        + " TEXT," +
                COLUMN_PATH         + " TEXT," +
                COLUMN_TIMES_PLAYED + " INTEGER DEFAULT 0," +
                COLUMN_RATING + " INTEGER DEFAULT 0" +
        ");";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SONGS + ";");
        onCreate(db);
    }

    //Add a new row to the database
    public void addSong (Songs song) {
        try {
            ContentValues values = new ContentValues();
            values.put(COLUMN_NAME, song.getName());
            values.put(COLUMN_ARTIST, song.getArtist());
            values.put(COLUMN_ALBUM, song.getAlbum());
            values.put(COLUMN_PATH, song.getPath());
            values.put(COLUMN_TIMES_PLAYED, song.getTimesPlayed());
            values.put(COLUMN_RATING, song.getRating());
            SQLiteDatabase db = getWritableDatabase();
            long id = db.insert(TABLE_SONGS, null, values);
            Log.i(TAG, "addSong: inserted " + id + ", name:" +  song.getName() + "["+song.getPath()+"]");
            db.close();
        } catch (Exception e) {
            Log.i(TAG, "addSong: exception" + e.toString());
        }
    }

    //Print the database as a string
    public String databaseToString () {
        String dbString = "";
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_SONGS + " WHERE 1";

        // Cursor points to a location in your results
        Cursor c = db.rawQuery(query, null);
        // Move to the first row
        c.moveToFirst();
        while (!c.isAfterLast()) {
            Log.i(TAG, "databaseToString: ");
            if (c.getString(c.getColumnIndex(COLUMN_PATH))!= null) {
                dbString += c.getString(c.getColumnIndex(COLUMN_PATH));
                Log.i(TAG, "databaseToString: song"+ c.getString(c.getColumnIndex(COLUMN_PATH)));
            }
            if (c.getString(c.getColumnIndex(COLUMN_TIMES_PLAYED))!= null) {
                dbString += " played " + c.getString(c.getColumnIndex(COLUMN_TIMES_PLAYED)) + " times ";
            }
            if (c.getString(c.getColumnIndex(COLUMN_RATING))!= null) {
                dbString += " HAVING " +  c.getString(c.getColumnIndex(COLUMN_RATING)) + " * as rating";
            }
            dbString += "\n";
            c.moveToNext();
        }
        Log.i(TAG, "databaseToString: song list" +  dbString);
        long cnt  = DatabaseUtils.queryNumEntries(db, TABLE_SONGS);
        Log.i(TAG, "databaseToString: Total of " +  cnt + " songs.");
        db.close();
        return dbString;
    }

    public ArrayList<Songs> getSongs ()
    {
        ArrayList<Songs> songs = new ArrayList<Songs>();
        SQLiteDatabase db = getWritableDatabase();

        String query = "SELECT * FROM " + TABLE_SONGS + " WHERE 1";
        // Cursor points to a location in your results
        Cursor c = db.rawQuery(query, null);
        // Move to the first row
        c.moveToFirst();
        while (!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex(COLUMN_PATH))!= null) {
                Songs song = new Songs();
                song.setName(c.getString(c.getColumnIndex(COLUMN_NAME)));
                song.setAlbum(c.getString(c.getColumnIndex(COLUMN_ALBUM)));
                song.setArtist(c.getString(c.getColumnIndex(COLUMN_ARTIST)));
                song.setPath(c.getString(c.getColumnIndex(COLUMN_PATH)));
                song.setTimesPlayed(c.getInt(c.getColumnIndex(COLUMN_TIMES_PLAYED)));
                song.setRating(c.getInt(c.getColumnIndex(COLUMN_RATING)));

                songs.add(song);
            }
            c.moveToNext();
        }
        return songs;

    }

    public ArrayList<Songs> getFilteredSongs (String sequence)
    {
        ArrayList<Songs> songs = new ArrayList<Songs>();
        SQLiteDatabase db = getWritableDatabase();

        String query = "SELECT * FROM " + TABLE_SONGS + " WHERE " + COLUMN_NAME + " LIKE '%" + sequence+ "%'" +
                " OR " + COLUMN_ARTIST + " LIKE '%"+ sequence+ "%' OR " +COLUMN_ALBUM +" LIKE '%"+sequence+"%'" ;
        // Cursor points to a location in your results
        Cursor c = db.rawQuery(query, null);
        // Move to the first row
        c.moveToFirst();
        while (!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex(COLUMN_PATH))!= null) {
                Songs song = new Songs();
                song.setName(c.getString(c.getColumnIndex(COLUMN_NAME)));
                song.setAlbum(c.getString(c.getColumnIndex(COLUMN_ALBUM)));
                song.setArtist(c.getString(c.getColumnIndex(COLUMN_ARTIST)));
                song.setPath(c.getString(c.getColumnIndex(COLUMN_PATH)));
                song.setTimesPlayed(c.getInt(c.getColumnIndex(COLUMN_TIMES_PLAYED)));
                song.setRating(c.getInt(c.getColumnIndex(COLUMN_RATING)));

                songs.add(song);
            }
            c.moveToNext();
        }
        return songs;

    }

    public void emptyDatabase()
    {
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM '" +  TABLE_SONGS + "' WHERE 1");
        db.close();
    }
}
