package adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

import ap.musicplayer.R;
import models.SongList;
import models.Songs;

public class SongListAdapter extends BaseAdapter {

    LayoutInflater inflater;
    private ArrayList<Songs> songs = new ArrayList<Songs>();

    public SongListAdapter (Context context, ArrayList<Songs> songs) {
        this.songs = songs;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return songs.size();
    }

    @Override
    public Object getItem(int position) {
        return songs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.song_list_layout, parent, false);
            viewHolder = new ViewHolder();
            viewHolder.songLabel = (TextView) convertView.findViewById(R.id.song_label);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Songs crtSong = this.songs.get(position);
        String label;
        if (crtSong.getName() != null && crtSong.getArtist() != null) {
            label = crtSong.getArtist() + " - " + crtSong.getName() + " [ " + crtSong.getAlbum() + " ]";
        } else {
            label = new File(crtSong.getPath()).getName();
        }

        viewHolder.songLabel.setText(label);

        return convertView;

    }


    private class ViewHolder {
        TextView songLabel;
    }


    public void updateData(ArrayList<Songs> songs) {
        this.songs = songs;
    }
}
