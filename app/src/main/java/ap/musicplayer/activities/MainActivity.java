package ap.musicplayer.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.MediaMetadataRetriever;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;

import java.io.File;
import java.lang.reflect.Array;
import java.util.ArrayList;

import ap.musicplayer.R;
import databases.SongsDBHandler;
import models.Songs;
import services.ScanMediaService;

public class MainActivity extends AppCompatActivity {

    private ProgressDialog progress;
    public SongsDBHandler dbHandler;
    private final static int MY_PERMISSIONS_REQUEST_READ_STORAGE = 101;

    public static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button scanMedia = (Button) findViewById(R.id.scan_media);
        scanMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askforPermissions();
                startScanMedia();
            }
        });

        Button displayMedia = (Button) findViewById(R.id.display_media);
        displayMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startDisplayMediaActivity();
            }
        });

        progress = new ProgressDialog(this);
        progress.setCancelable(false);
        progress.setMessage("Loading");

        dbHandler = new SongsDBHandler(this, null, null, 1);

    }

    public void startScanMedia()
    {

        // first implementation used a Service
        //Intent service = new Intent(this, ScanMediaService.class);
        //this.startService(service);


        // 2nd implementation used async task
        showProgress();
        new ScanMediaAsync().execute();

    }

    public void startDisplayMediaActivity()
    {
        Intent listMedia = new Intent(this, DisplayMedia.class);
        startActivity(listMedia);
    }

    private void showProgress() { progress.show(); }

    private void hideProgress() { progress.dismiss(); }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        hideProgress();
    }

    private void askforPermissions () {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE  },
                        MY_PERMISSIONS_REQUEST_READ_STORAGE )  ;

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    this.finish();
                    System.exit(0);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }



    private class ScanMediaAsync extends AsyncTask<String, Integer, String>
    {

        private static final String MP3_PATTERN = ".mp3";
        private static final String FLAC_PATTERN = ".flac";
        private static final String WAVE_PATTERN = ".wav";
        //private final String ROOT_DIR_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
        private final String ROOT_DIR_PATH = "/";

        public ArrayList<String> files = new ArrayList<String>();


        public ScanMediaAsync() {
            super();
        }

        @Override
        protected String doInBackground(String... params) {
            dbHandler.emptyDatabase();
            Log.i(TAG, "doInBackground:" +ROOT_DIR_PATH);
            scanDirectory(ROOT_DIR_PATH);
            return "";
        }

        private void scanDirectory(String path)
        {
            Log.i(TAG, "scanDirectory: Begin " +  path);
            File pathDir =  new File(path);
            File[] allFilesInDir = pathDir.listFiles();
            Log.i(TAG, "scanDirectory: has files" + allFilesInDir.length);
            MediaMetadataRetriever mmr = new MediaMetadataRetriever();
            if (allFilesInDir instanceof File[] && (allFilesInDir != null && allFilesInDir.length > 0)) {
                for (int i = 0; i < allFilesInDir.length; i++) {
                    if (allFilesInDir[i].getAbsolutePath().startsWith("/sys/") || allFilesInDir[i].getAbsolutePath().startsWith("/system/") ||  allFilesInDir[i].getAbsolutePath().startsWith("/proc/")) {
                        continue;
                    }
                    if (allFilesInDir[i].isDirectory()) {
                        try {
                            scanDirectory(allFilesInDir[i].getAbsolutePath());
                            Log.i(TAG, "scanDirectory: " +  allFilesInDir[i].getAbsolutePath() + " is directory " +allFilesInDir[i].isDirectory());
                        } catch (Exception e) {
                            Log.i(TAG, "scanDirectory: " + e.toString());
                        }
                    } else if (
                            allFilesInDir[i].getName().endsWith(MP3_PATTERN)  ||
                            allFilesInDir[i].getName().endsWith(FLAC_PATTERN) ||
                            allFilesInDir[i].getName().endsWith(WAVE_PATTERN)
                    )
                    {
                        Log.i(TAG, "scanDirectory: file found " + allFilesInDir[i].getName());

                        Log.i(TAG, "scanDirectory: zzz"+allFilesInDir[i].getName());
                        mmr.setDataSource(allFilesInDir[i].getAbsolutePath());

                        String albumName = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
                        String artistName = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
                        String songTitle = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);

                        Songs song = new Songs();
                        song.setAlbum(albumName);
                        song.setArtist(artistName);
                        song.setName(songTitle);
                        song.setPath(allFilesInDir[i].getAbsolutePath());
                        song.setTimesPlayed(0);
                        song.setRating(0);
                        dbHandler.addSong(song);
                    }
                }
            }
        }


        @Override
        protected void onPostExecute(String s) {
            hideProgress();
            super.onPostExecute(s);
        }
    }



}
