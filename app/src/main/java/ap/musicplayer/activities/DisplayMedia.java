package ap.musicplayer.activities;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.LoginFilter;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.util.ArrayList;

import adapters.SongListAdapter;
import ap.musicplayer.R;
import databases.SongsDBHandler;
import models.Songs;

public class DisplayMedia extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private static final int  OFFSET_SKIP_TO_PREV = 3000;
    private ArrayList<Songs> songs = new ArrayList<Songs>();
    private MediaPlayer mp;
    private String formerSong;
    private SeekBar seekBar;
    private int currentSong = -1;
    ListView sv;
    AudioManager am;
    SeekBar volumeSeekBar;
    EditText searchText;
    SongsDBHandler dbHandler;
    private static final int MIN_SEARCH_STRING_LENGTH = 3;
    SongListAdapter songListAdapter;
    private Button next;

    public static final String TAG = DisplayMedia.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_media);

        dbHandler = new SongsDBHandler(this, null, null, 1);
        songs = dbHandler.getSongs();

        songListAdapter = new SongListAdapter(this, songs);
        sv = (ListView) findViewById(R.id.lv_songs);
        sv.setAdapter(songListAdapter);
        sv.setOnItemClickListener(this);



        seekBar = (SeekBar) findViewById(R.id.seekBar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (mp.isPlaying()) {
                    if (fromUser == true) {
                        mp.seekTo(progress);
                    }
                } else {
                    resetProgress();
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        final Handler handler = new Handler();
        Runnable runnable = new Runnable() {

            @Override
            public void run() {
                try{
                    if (mp != null) {
                        if (mp.isPlaying()) {
                            int currentProgress = mp.getCurrentPosition();
                            seekBar.setProgress(currentProgress);
                        }
                    }
                }
                catch (Exception e) {
                    // TODO: handle exception
                }
                finally{
                    //also call the same runnable to call it at regular interval
                    handler.postDelayed(this, 1000);
                }
            }
        };
        handler.postDelayed(runnable, 1000);


        Button play = (Button) findViewById(R.id.play_button);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchText.setVisibility(View.INVISIBLE);
                if (mp!= null && !mp.isPlaying()) {
                    int progress_current = mp.getCurrentPosition();
                    mp.seekTo(progress_current);
                    mp.start();
                    setSongInfo();
                } else {
                    //mp.release();
                }
            }
        });

        Button pause = (Button) findViewById(R.id.pause_button);
        pause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchText.setVisibility(View.INVISIBLE);
                if (mp!= null && mp.isPlaying()) {
                    mp.pause();
                }
            }
        });

        Button stop = (Button) findViewById(R.id.stop_button);
        stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchText.setVisibility(View.INVISIBLE);
                if (mp!= null) {
                    mp.pause();
                    mp.seekTo(0);
                    resetProgress();
                }
            }
        });

        final Button prev = (Button) findViewById(R.id.first_button);
        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchText.setVisibility(View.INVISIBLE);
                if (mp!= null) {
                    if (mp.getCurrentPosition() > OFFSET_SKIP_TO_PREV) {
                        mp.pause();
                        mp.seekTo(0);
                        mp.start();
                        setSongInfo();
                        playNextSongAfterCurrentFinishes();
                        resetProgress();
                    } else if (currentSong != -1) {
                        int prevSong = currentSong - 1;
                        if (prevSong < 0) {
                            prevSong = 0;
                        }
                        sv.performItemClick(sv, prevSong, sv.getItemIdAtPosition(prevSong));
                    }
                }
            }
        });

        next = (Button) findViewById(R.id.last_button);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchText.setVisibility(View.INVISIBLE);
                volumeSeekBar.setVisibility(View.INVISIBLE);
                if (mp!= null) {
                    int nextSong = currentSong + 1;
                    int totalSongs = sv.getAdapter().getCount();
                    //Toast.makeText(DisplayMedia.this, totalSongs, Toast.LENGTH_LONG).show();

                    if (nextSong >= totalSongs - 1) {
                        nextSong = totalSongs - 1;
                    }
                    sv.performItemClick(sv, nextSong, sv.getItemIdAtPosition(nextSong));
                }
            }
        });

        volumeSeekBar = (SeekBar) findViewById(R.id.volume_controller);
        volumeSeekBar.setBackground(new ColorDrawable(Color.parseColor("#EBF1F2")));
        volumeSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (fromUser == true) {
                    am.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Button volume = (Button) findViewById(R.id.volume_button);
        volume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                am = (AudioManager) getSystemService(AUDIO_SERVICE);
                int volumeLevel= am.getStreamVolume(AudioManager.STREAM_MUSIC);
                int maxVolumeLevel = am.getStreamMaxVolume(AudioManager.STREAM_MUSIC);

                volumeSeekBar.setMax(maxVolumeLevel);
                volumeSeekBar.setProgress(volumeLevel);

                if (volumeSeekBar.getVisibility() == View.VISIBLE) {
                    volumeSeekBar.setVisibility(View.INVISIBLE);
                } else {
                    volumeSeekBar.setVisibility(View.VISIBLE);
                }
            }
        });

        final Button searchButton = (Button) findViewById(R.id.searchButton);
        searchText = (EditText) findViewById(R.id.searchText);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (searchText.getVisibility() == View.VISIBLE) {
                    searchText.setVisibility(View.INVISIBLE);
                } else {
                    searchText.setVisibility(View.VISIBLE);
                    searchText.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.showSoftInput(searchText, InputMethodManager.SHOW_IMPLICIT);
                }
            }
        });

       searchText.addTextChangedListener(new TextWatcher() {
           @Override
           public void beforeTextChanged(CharSequence s, int start, int count, int after) {

           }

           @Override
           public void onTextChanged(CharSequence s, int start, int before, int count) {
               if (s.toString().length() >= MIN_SEARCH_STRING_LENGTH) {
                   filterSongs(s.toString());
               } else {
                   songs = dbHandler.getSongs();
                   songListAdapter.updateData(songs);
                   songListAdapter.notifyDataSetChanged();
               }
           }

           @Override
           public void afterTextChanged(Editable s) {

           }
       });

    }

    private void filterSongs(String sequence)
    {
        songs = dbHandler.getFilteredSongs(sequence);
        songListAdapter.updateData(songs);
        songListAdapter.notifyDataSetChanged();

    }



    private void resetProgress ()
    {
        seekBar.setMax(mp.getDuration());
        seekBar.setProgress(0);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        searchText.setVisibility(View.INVISIBLE);
        volumeSeekBar.setVisibility(View.INVISIBLE);

        String songPath = songs.get(position).getPath();
        this.currentSong = position;
        if (this.mp == null) {
            this.mp = MediaPlayer.create(this, Uri.parse(songPath));
        } else {
            if (mp.isPlaying() && formerSong != songPath) {
                mp.stop();
                this.mp = MediaPlayer.create(this, Uri.parse(songPath));
            }
            if (!mp.isPlaying()) {
                this.mp.release();
                this.mp = MediaPlayer.create(this, Uri.parse(songPath));
            }
        }
        resetProgress();
        mp.start();
        setSongInfo();
        this.formerSong = songPath;
        playNextSongAfterCurrentFinishes();
    }

    private void playNextSongAfterCurrentFinishes()
    {
        Log.i(TAG, "playNextSongAfterCurrentFinishes: " + mp.toString());
        if (mp != null) {
            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if (currentSong <= songs.size() - 1) {
                        next.performClick();
                    }
                }
            });
        }
    }


    private void setSongInfo ()
    {
        String songPath = songs.get(currentSong).getPath();
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(songPath);

        String albumName = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
        String artistName = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        String songTitle = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
        String info = songTitle + " - " + artistName + "["+albumName+"]";

        if (albumName == null && songTitle == null && artistName == null) {
            info = new File(songPath).getName();
        }

        TextView tv = (TextView) findViewById(R.id.song_info);
        tv.setText(info);
    }

}
